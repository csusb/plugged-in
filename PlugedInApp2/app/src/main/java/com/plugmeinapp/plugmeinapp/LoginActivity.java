package com.plugmeinapp.plugmeinapp;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import android.app.ProgressDialog;
import android.content.Intent;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity{

    private ProgressDialog progressDialog; // shows loading

    private Button buttonSignin;          // sign in button
    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextView textViewSignUp;

    private FirebaseAuth firebaseAuth;

    private DatabaseReference mUserDatabase;
    private FirebaseUser mCurrentUser;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        // if currentUser is there, take this to Main activity
       /* if(firebaseAuth.getCurrentUser() != null){

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }*/

        // attach the variable ID to the layout
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonSignin = (Button) findViewById(R.id.buttonSignin);
        textViewSignUp = (TextView) findViewById(R.id.textViewSignUp);

        progressDialog = new ProgressDialog(this);

        // when signin button is clicked, follow the userLogin method to sign in the regarding information
        buttonSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
            }
        });

        // if signup, take user to the choose what type of user they want to sign up
        textViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(LoginActivity.this, ChooseUserTypeActivity.class));
                finish();
            }
        });
    }

    // details of user login check method
    private void userLogin(){

        // values assigned from the input from User
        String email = editTextEmail.getEditableText().toString();
        String password = editTextPassword.getEditableText().toString();

        // if the value input is empty for email
        if(TextUtils.isEmpty(email)){
            //ask for the input email
            Toast.makeText(this, "Please Enter Your Email", Toast.LENGTH_SHORT).show();
            //stopping the function
        }

        // if the value input is empty for password
        if(TextUtils.isEmpty(password)) {
            //ask for the input password
            Toast.makeText(this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
        }else if(password.startsWith("0")){ // if the input starts with 0
            //no 0 inputs
                Toast.makeText(this, "Please Enter a Right Password", Toast.LENGTH_SHORT).show();
                return;
        }

        // if the email and passsword is not empty
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
            Log.d("Checking EditText ...","Email: " + email);
            // display following for loading screen so that user sees it process
            progressDialog.setMessage("Logging In...");
            progressDialog.show();
            Log.d("I am here","OK");
            firebaseAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    Log.d("I am here","OK");

                    // get the datasnapshot of the current User access, in order to show either app layout of  "Artist" or "Listener"
                    mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
                    String current_uid = mCurrentUser.getUid();
                    mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(current_uid);
                    mUserDatabase.keepSynced(true);

                    mUserDatabase.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                         final String usertype = dataSnapshot.child("userType").getValue().toString(); // get the usertype according to input given from User

                            // if the usertype is 1 as Artist
                            if(usertype.equals("1")) {
                                progressDialog.dismiss();

                                // take the Artist layout to decide to open a chat or edit profile
                                Intent i = new Intent(LoginActivity.this, ArtistStartChatActivity.class);
                                startActivity(i);
                                finish();
                            }
                            // if the userType is 2 as Listener
                            if(usertype.equals("2")) {
                                progressDialog.dismiss();

                                // take the user to Main user page
                                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            });
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}