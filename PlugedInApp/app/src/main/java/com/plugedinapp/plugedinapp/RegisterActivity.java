package com.plugedinapp.plugedinapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import android.graphics.drawable.AnimationDrawable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import android.app.ProgressDialog;

public class RegisterActivity extends AppCompatActivity {

    //private Toolbar mToolBar;


    private RelativeLayout relativeLayout;
    private AnimationDrawable animationDrawable;

    private EditText registerUerName;
    private EditText registerEmail;
    private EditText registerPassword;
    private Button createAccountBtn;
    private ProgressDialog loadingBar;

    private FirebaseAuth mAuth;
    private DatabaseReference storeUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

       registerUerName = (EditText) findViewById(R.id.register_name);
       registerEmail = (EditText) findViewById(R.id.register_email);
       registerPassword = (EditText) findViewById(R.id.register_password);
       createAccountBtn = (Button) findViewById(R.id.create_account_btn);
       loadingBar = new ProgressDialog(this);

       mAuth = FirebaseAuth.getInstance();

       createAccountBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v)
           {
              final String name = registerUerName.getText().toString();
               String email = registerEmail.getText().toString();
               String password = registerPassword.getText().toString();

               RegisterAccount(name, email, password);
           }
       });

        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        animationDrawable = (AnimationDrawable) relativeLayout.getBackground();
        animationDrawable.setExitFadeDuration(5000);
    }

    private void RegisterAccount(final String name, String email, String password)
    {
        if(TextUtils.isEmpty(name))
        {
            registerUerName.requestFocus();
            Toast.makeText(RegisterActivity.this, "Please Enter a Name", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(email))
        {
            registerEmail.requestFocus();
            Toast.makeText(RegisterActivity.this, "Please Enter Your Email", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(password))
        {
            registerPassword.requestFocus();
            Toast.makeText(RegisterActivity.this, "Please Enter a Password", Toast.LENGTH_SHORT).show();
            return;
        }else if(password.startsWith("0")){
            registerPassword.requestFocus();
            Toast.makeText(this, "Enter a valid password", Toast.LENGTH_SHORT).show();
            return;
        }
        else
        {
            loadingBar.setTitle("Creating new account");
            loadingBar.setMessage("Please wait");
            loadingBar.show();

            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task)
                {
                    if(task.isSuccessful())
                    {
                       String current_userId =  mAuth.getCurrentUser().getUid();
                        storeUserData = FirebaseDatabase.getInstance().getReference().child("Artist").child(current_userId);

                        storeUserData.child("username").setValue(name);
                        storeUserData.child("status").setValue("Hey There!");
                        storeUserData.child("user_image").setValue("default_avatar");
                        storeUserData.child("user_thumb_image").setValue("default_avatar").addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task)
                            {
                                if(task.isSuccessful())
                                {
                                    Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(mainIntent);
                                    finish();
                                }

                            }
                        });
                    }
                    else
                    {
                        Toast.makeText(RegisterActivity.this, "Error Couldn't Register, Try Again!", Toast.LENGTH_SHORT).show();
                    }
                    loadingBar.dismiss();

                }
            });
        }


    }
    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            // start the animation
            animationDrawable.start();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            // stop the animation
            animationDrawable.stop();
        }
    }

}
