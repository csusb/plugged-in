package com.plugmeinapp.plugmeinapp;

// this activity decides Usertype for Newly registered User: Artist or Listener

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class ChooseUserTypeActivity extends AppCompatActivity {
    private static final String TAG = ChooseUserTypeActivity.class.getSimpleName();
    private Button btn_user_selection_vj, btn_user_selection_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_user_type);

        //attach the variables to the xml layout
        btn_user_selection_user = findViewById(R.id.btn_user_selection_user);
        btn_user_selection_vj = findViewById(R.id.btn_user_selection_vj);


        // following code assigns the usertype depending on the User choice so that they can go onto register activity to fill out the email password details
        btn_user_selection_vj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChooseUserTypeActivity.this, RegisterActivity.class);
                String userType = "1"; // artist
                i.putExtra("userType",userType);
                startActivity(i);
                finish();
            }
        });
        btn_user_selection_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChooseUserTypeActivity.this, RegisterActivity.class);
                String userType = "2"; // listener
                i.putExtra("userType",userType);
                startActivity(i);
                finish();
            }
        });
    }
}
