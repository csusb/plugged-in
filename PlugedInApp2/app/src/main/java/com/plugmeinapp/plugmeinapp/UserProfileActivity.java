package com.plugmeinapp.plugmeinapp;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity {
    public static final int RESULT_LOAD_IMAGE = 1;

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mRootRef, mUserRef;

    private TextView textViewUserTitle;
    private TextView textViewUserEmail;

    private EditText editTextUserBio;

    private CircleImageView circleImageView;

    private InputMethodManager mgr;

    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Main Menu");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textViewUserTitle = findViewById(R.id.textView_UserProfile_Title);
        textViewUserEmail = findViewById(R.id.textView_UserProfile_Email);

        editTextUserBio = findViewById(R.id.editText_Bio);

        try {
            mAuth = FirebaseAuth.getInstance();
            mCurrentUser = mAuth.getCurrentUser();

            if (mAuth == null || mCurrentUser == null) {
                Intent i = new Intent(UserProfileActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        mRootRef = FirebaseDatabase.getInstance().getReference();
        mUserRef = mRootRef.child("Users");

        mgr =  (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        mUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUserRef.child(mCurrentUser.getUid());
             }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        editTextUserBio.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))){
                    String bio_message_to_fb = editTextUserBio.getEditableText().toString();
                    mUserRef.child(mCurrentUser.getUid()).child("bio").setValue(bio_message_to_fb);
                    mgr.hideSoftInputFromWindow(editTextUserBio.getWindowToken(), 0);
                    return true;
                } else{
                    return false;
                }
            }
        });

        circleImageView = (CircleImageView)findViewById(R.id.circleImageView_profile_image);
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULT_LOAD_IMAGE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data){
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            textViewUserEmail.setText(picturePath);
            circleImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }
}
