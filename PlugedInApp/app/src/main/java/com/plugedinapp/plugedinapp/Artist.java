package com.plugedinapp.plugedinapp;

public class Artist {

    public String status;

    public String username;

    public String image;

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String online;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }




    public Artist()
    {

    }

    public Artist(String status, String username, String online) {
        this.status = status;
        this.username = username;
        this.online = online;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
