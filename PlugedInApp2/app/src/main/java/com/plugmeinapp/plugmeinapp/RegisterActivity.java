package com.plugmeinapp.plugmeinapp;

// this activity is responsible for the register of the New user after the user decided to be Artist type or Listener type
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity{
    private static final String TAG = RegisterActivity.class.getSimpleName();

    // firebase variables to access user info from the Firebase Database
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private FirebaseFirestore mDb;

    // data reference variables to point
    private DatabaseReference mRef, mUserRef;

    // variables to edit page
    private Button buttonRegister;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextTitle;
    private TextView textViewSignIn;
    private ProgressDialog progressDialog;

    private Intent intent;
    private String userType="";
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) { // at create phase
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // assign to get the authorization
        mAuth = FirebaseAuth.getInstance();

        // if the user access is currently not null,
        if(mAuth.getCurrentUser() != null){

            // take the user to main activity page
            startActivity(new Intent(RegisterActivity.this, MainActivity.class));
            finish();
        }

        mDb = FirebaseFirestore.getInstance();

        mRef = FirebaseDatabase.getInstance().getReference();
        mUserRef = mRef.child("Users");

        progressDialog = new ProgressDialog(this);

        //attach variables to layout
        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextTitle = (EditText)findViewById(R.id.editTextTitle);
        textViewSignIn = (TextView) findViewById(R.id.textViewSignin);
    }

    protected void onStart(){
        super.onStart();

        intent = getIntent();
        try{// receive user type from the previous ChooseUserTypeActivity
            userType = intent.getExtras().getString("userType");
        } catch(NullPointerException e){
            e.printStackTrace();
            Toast.makeText(this, "userType is null", Toast.LENGTH_SHORT).show();
        }
        //after the register button is clicked, take the user with its type finish registering
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser(userType);
            }
        });
        // go back to login activity
        textViewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }
        });
    }

    // details of registering user according the its type

    private void registerUser(final String userType) {

        // email, password, title details are collected here
        final String email = editTextEmail.getEditableText().toString().trim();
        String password = editTextPassword.getEditableText().toString().trim();
        final String title = editTextTitle.getEditableText().toString();


        // empty email, password, nickname(ID) of the user: if not given asks for the input as error message
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this, "Please Enter Your Email", Toast.LENGTH_SHORT).show();
            //stopping the function
            return;
        }
        if(TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
        }else if(password.startsWith("0")){
            Toast.makeText(this, "Please Enter a Password Greater than 0", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(title)){
            Toast.makeText(this, "Please Enter Your Title", Toast.LENGTH_SHORT).show();
            return;
        }



        // if everything is successfully inputed(email, password, nickname), now onto registering user
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(title)){
            // dialog for loading for user to see it is processing
            progressDialog.setMessage("Registering User...");
            progressDialog.show();

            //objectify user to the database using hashmap to Put the User details to the Hashmap
            mAuth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                mCurrentUser = mAuth.getCurrentUser();
                                DatabaseReference mCurrentUserRef = mUserRef.child(mCurrentUser.getUid());

                                //User user = new User(title,email,userType);

                                Map<String, Object> user = new HashMap<>();
                                user.put("title", title);
                                user.put("email",email);
                                user.put("userType", userType);

                                mCurrentUserRef.setValue(user);

                                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                finish();
                            }else{
                                // if the registration is not successful, display error message
                                Toast.makeText(RegisterActivity.this, "Couldn't Register Passwords Most 6+ ", Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        }
                    });
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}