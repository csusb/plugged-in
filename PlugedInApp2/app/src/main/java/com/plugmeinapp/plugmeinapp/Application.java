package com.plugmeinapp.plugmeinapp;

import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Application {
    FirebaseAuth mAuth;          // Firebase authorization variable
    FirebaseUser mCurrentUser;   // Firebase CurrentUser Info access variable

    public boolean firebaseInit(){
        try {
            mAuth = FirebaseAuth.getInstance();       // retrieve info about current user
            mCurrentUser = mAuth.getCurrentUser();

            if (mAuth == null && mCurrentUser == null) { // if the current user turned out to be null
                return true;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
