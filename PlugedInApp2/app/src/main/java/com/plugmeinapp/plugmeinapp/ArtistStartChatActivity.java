package com.plugmeinapp.plugmeinapp;

// this activity deals with Usertype:Artist to create chat room or edit profile

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ArtistStartChatActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mRef, mUsersRef, mCurrentUserRef, mChatsRef;

    private Button button_Artist_startChat, button_Artist_EditProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_start_chat);    // set to xml file layout

        // initialize to get current user's information
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mRef = FirebaseDatabase.getInstance().getReference();
        mUsersRef = mRef.child("Users");
        mChatsRef = mRef.child("Chats");

        try {// this is created so that user authorization is empty(turned off app, signed out to come back)
            mAuth = FirebaseAuth.getInstance();
            mCurrentUser = mAuth.getCurrentUser();

            // if currentUser turned out to be null now Start from login activity
            if (mAuth == null && mCurrentUser == null) {
                Intent i = new Intent(ArtistStartChatActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        // choose either chat or edit profile for Artist option
        button_Artist_startChat = findViewById(R.id.button_Artist_startChat);
        button_Artist_EditProfile = findViewById(R.id.button_Artist_EditProfile);

        button_Artist_startChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startArtistChat = new Intent(ArtistStartChatActivity.this, LoginActivity.class);
            }
        });

        // when user clicks to go edit UserProfile, move the page to the UserProfile activity
        button_Artist_EditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent j = new Intent(ArtistStartChatActivity.this, UserProfileActivity.class);
                startActivity(j);
                finish();
            }
        });
    }
}
