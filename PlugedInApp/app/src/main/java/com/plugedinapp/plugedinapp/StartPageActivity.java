package com.plugedinapp.plugedinapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.graphics.drawable.AnimationDrawable;
import android.widget.RelativeLayout;

public class StartPageActivity extends AppCompatActivity {

    private Button needNewAccountBtn;
    private Button alredyHaveAccountBtn;
    private Button needNewAccountBtn2;


    private RelativeLayout relativeLayout;
    private AnimationDrawable animationDrawable;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);

        needNewAccountBtn = (Button) findViewById(R.id.need_account_btn);
        alredyHaveAccountBtn = (Button) findViewById(R.id.already_have_account_btn);
        needNewAccountBtn2 = (Button) findViewById(R.id.need_account_btn2);

        needNewAccountBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartPageActivity.this, UserRegisterActivity.class);
                startActivity(intent);
            }
        });

        needNewAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(StartPageActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        });

        alredyHaveAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(StartPageActivity.this, LoginActivity.class);
                startActivity(loginIntent);
            }
        });

        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        animationDrawable = (AnimationDrawable) relativeLayout.getBackground();
        animationDrawable.setExitFadeDuration(5000);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            // start the animation
            animationDrawable.start();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            // stop the animation
            animationDrawable.stop();
        }
    }
}
