package com.plugedinapp.plugedinapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {

    private String messageReceiverId;
    private String messageReceiverName;

    private Toolbar chatToolBar;
    private TextView userNameTitle;
    private TextView lastSeen;
    private CircleImageView chatProfileImage;
    private ImageButton sendMessageBtn;
    private EditText inputMessageText;

    private DatabaseReference rootRef;
    private FirebaseAuth mAuth;
    private String messageSenderId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        rootRef = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        messageSenderId = mAuth.getCurrentUser().getUid();


        sendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               SendMessage();
            }
        });

      // messageReceiverId = getIntent().getExtras().get("visit_user_id").toString();
       // messageReceiverName = getIntent().getExtras().get("visit_user_id").toString();

        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View action_bar_view = layoutInflater.inflate(R.layout.chat_custom_bar, null);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setCustomView(action_bar_view);


        userNameTitle = (TextView) findViewById(R.id.custom_prfoile_name);
        lastSeen = (TextView) findViewById(R.id.custom_last_seen);
        chatProfileImage = (CircleImageView) findViewById(R.id.custom_profile_image);

        sendMessageBtn = (ImageButton) findViewById(R.id.send_message_btn);
        inputMessageText = (EditText) findViewById(R.id.input_message);



       // userNameTitle.setText(messageReceiverName);


        /*rootRef.child("Artist").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                final String online = dataSnapshot.child("online").getValue().toString();
                //final String userThumb = dataSnapshot.child("thumb_image").getValue().toString();


                if(online == "true")
                {
                    lastSeen.setText("online");
                }
                else
                {
                    lastSeen.setText("offline");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/


    }

    private void SendMessage()
    {
        String messageText = inputMessageText.getText().toString();

        if(TextUtils.isEmpty(messageText))
        {
            Toast.makeText(ChatActivity.this, "Please write Your Message", Toast.LENGTH_SHORT).show();
        }
        else
        {
            String message_sender_ref = "Sender_Messages";
            String message_receiver_ref = "Receiver_Messages";

            DatabaseReference userMessageKey = rootRef.child("Message").child("Sender_Messages").child("Receiver_Messages").push();

            String message_push_id = userMessageKey.getKey();

            Map messageTextBody = new HashMap();

            messageTextBody.put("message", messageText);
            messageTextBody.put("type", "text");

            Map messageDetalis = new HashMap();

        }
    }
}
