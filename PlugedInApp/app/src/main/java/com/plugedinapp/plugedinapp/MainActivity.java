package com.plugedinapp.plugedinapp;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import android.widget.Toolbar;
import android.graphics.drawable.AnimationDrawable;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity {


    //private android.support.v7.widget.Toolbar mToolBar;

    private RelativeLayout relativeLayout;
    private AnimationDrawable animationDrawable;

    private RecyclerView allArtistList;
    private FirebaseAuth mAuth;
    private DatabaseReference allArtistRef;
    private FirebaseUser currentUser;
    private DatabaseReference userRef;

    //private ViewPager myViewPager;
    //private TabLayout myTabLayout;
    //private TabsPagerAdapter myTabsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*mToolBar = (android.support.v7.widget.Toolbar) findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setTitle("myChat");*/

       /* myViewPager = (ViewPager) findViewById(R.id.main_tabs_pager);
        myTabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        myViewPager.setAdapter(myTabsPagerAdapter);
        myTabLayout = (TabLayout) findViewById(R.id.main_tabs);
        myTabLayout.setupWithViewPager(myViewPager);*/



       allArtistList = (RecyclerView) findViewById(R.id.all_artist_list);
       allArtistList.setHasFixedSize(true);
       allArtistList.setLayoutManager(new LinearLayoutManager(this));

       mAuth = FirebaseAuth.getInstance();

        currentUser = mAuth.getCurrentUser();

        if(currentUser != null)
        {
            String online_user_id = mAuth.getCurrentUser().getUid();
            userRef = FirebaseDatabase.getInstance().getReference().child("Artist").child(online_user_id);
        }

        allArtistRef = FirebaseDatabase.getInstance().getReference().child("Artist");
        allArtistRef.keepSynced(true);

        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        animationDrawable = (AnimationDrawable) relativeLayout.getBackground();
        animationDrawable.setExitFadeDuration(5000);


    }

    @Override
    protected void onStart()
    {
        super.onStart();

        FirebaseRecyclerAdapter<Artist, allArtistViewHolder> firebaseRecyclerAdapter
                = new FirebaseRecyclerAdapter<Artist, allArtistViewHolder>
                (
                        Artist.class, R.layout.all_artist_display_layout, allArtistViewHolder.class, allArtistRef
                )
        {
            @Override
            protected void populateViewHolder(final allArtistViewHolder viewHolder, Artist model, final int position)
            {
                viewHolder.setUsername(model.getUsername());
                viewHolder.setStatus(model.getStatus());
                viewHolder.setOnline(model.getOnline());

               /* allArtistRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        if(dataSnapshot.hasChild("online"))
                        {
                            String online = (String) dataSnapshot.child("online").getValue();
                            viewHolder.setOnline(online);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });*/



                //final String visit_user_id = getRef(position).getKey();


                /*viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        CharSequence options[] = new CharSequence[]
                                {
                                        "User Profile",
                                        "Go to Chat"
                                };

                        AlertDialog.Builder builder = new AlertDialog.Builder(getBaseContext());
                        builder.setTitle("Select Option");

                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int postion)
                            {
                                if (position == 0)
                                {
                                    Intent profileIntent = new Intent(getBaseContext(), ProfileActivity.class);
                                    profileIntent.putExtra("visit_user_id", visit_user_id);
                                    startActivity(profileIntent);
                                }

                                if (position == 1)
                                {
                                    Intent chatIntent = new Intent(getBaseContext(), ChatActivity.class);
                                    chatIntent.putExtra("visit_user_id", visit_user_id);
                                    startActivity(chatIntent);
                                }
                            }
                        });
                        builder.show();
                    }
                });*/



                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        String visit_user_id = getRef(position).getKey();

                        Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                        profileIntent.putExtra("visit_user_id", visit_user_id);
                        startActivity(profileIntent);
                    }
                });

            }
        };

        allArtistList.setAdapter(firebaseRecyclerAdapter);


        //currentUser = mAuth.getCurrentUser();


        if(currentUser == null)
        {
            LogOutUser();
        }
        else if (currentUser != null)
        {
            userRef.child("online").setValue("true");
        }
    }

   @Override
    protected void onStop()
    {
        super.onStop();

        if (currentUser != null)
        {
            userRef.child("online").setValue("false");
        }
    }

    public static class allArtistViewHolder extends RecyclerView.ViewHolder
    {
        View mView;

        public allArtistViewHolder(View itemView)
        {
            super(itemView);

            mView = itemView;
        }

        public void setUsername(String username)
        {
            TextView name = (TextView) mView.findViewById(R.id.all_artist_username);
            name.setText(username);
        }

        public void setStatus(String status)
        {
            TextView mstatus = (TextView) mView.findViewById(R.id.all_artist_status);
            mstatus.setText(status);
        }

        public void setOnline(String online)
        {
            ImageView onlineStatus = (ImageView) mView.findViewById(R.id.online_status);

            if(Objects.equals(online, "true"))
            {
                onlineStatus.setVisibility(View.VISIBLE);
            }
            else
            {
                onlineStatus.setVisibility(View.INVISIBLE);
            }
        }

        /*public void setImage(String image)
        {
            CircleImageView mImage = (CircleImageView) mView.findViewById(R.id.all_artist_profile_image);

        }*/
    }

    private void LogOutUser()
    {

        Intent startPageIntent = new Intent(MainActivity.this, StartPageActivity.class);
        startPageIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(startPageIntent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.main_logout_btn)
        {
            mAuth.signOut();
            LogOutUser();
        }

        if(item.getItemId() == R.id.main_accountSettings_btn)
        {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }
        return true;
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (animationDrawable != null && !animationDrawable.isRunning()) {
            // start the animation
            animationDrawable.start();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (animationDrawable != null && animationDrawable.isRunning()) {
            // stop the animation
            animationDrawable.stop();
        }
    }
}
