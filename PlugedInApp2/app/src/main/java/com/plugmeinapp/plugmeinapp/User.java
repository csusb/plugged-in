package com.plugmeinapp.plugmeinapp;

public class User {
    private String title;
    private String email;
    private String userType;

    public User(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String title, String email, String userType){
        this.title = title;
        this.email = email;
        this.userType = userType;
    }
    public String getUserTitle(){
        return title;
    }
    public String getUserEmail(){
        return email;
    }
    public String getUserType(){
        return userType;
    }

    public void setUserTitle(String title){
        this.title = title;
    }

    public void setUserEmail(String email){
        this.email = email;
    }

    public void setUserType(String userType){
        this.userType = userType;
    }
}
