package com.plugmeinapp.plugmeinapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ArrayAdapter;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.firebase.firestore.QuerySnapshot;
import com.plugmeinapp.plugmeinapp.User;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    /*  implements View.OnClickListener*/

    // declare firebase variables to access the Firebase databse
    private FirebaseAuth firebaseAuth;
    private FirebaseUser mCurrentUser;
    private FirebaseFirestore mDb;
    private DatabaseReference mRef, mUsersRef, mChatsRef, mCurrentUserRef;
    //private ListView mListView;

    private TextView textViewUserEmail;
    private Button buttonLogout;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    private String uId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //mListView = (ListView) findViewById(R.id.listView);


        // toolbar layout declaration
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Main Menu");
        // Adding Back Button on AppBar.
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //
        try {

            //assign firebase variable to get the current user info
            firebaseAuth = FirebaseAuth.getInstance();
            mCurrentUser = firebaseAuth.getCurrentUser();

            // check current user is null so that it will take the user screen to login page to sign in again
            if (firebaseAuth == null && mCurrentUser == null) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }

        //declare reference for eht chat use
        mRef = FirebaseDatabase.getInstance().getReference();

        mUsersRef = mRef.child("Users");
        mChatsRef = mRef.child("Chats");

        uId = mCurrentUser.getUid();

        // if there is update on the data change about the chat
        try{
            mUsersRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    showData(dataSnapshot);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        catch(NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    // following switch statement was created so that User:listener can sign out/update profile from the main page.
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        switch(item.getItemId()){
            case R.id.signOut:
                firebaseAuth.signOut();
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
                return true;
            case R.id.menu_item_editProfile:
                Intent j = new Intent(MainActivity.this, UserProfileActivity.class);
                startActivity(j);
                return true;
            default:
                return false;
        }
    }

    // following code is to display the data of the User
    private void showData(DataSnapshot dataSnapshot){
            for(DataSnapshot ds : dataSnapshot.getChildren()){
                User user = new User();
                ds.getValue(User.class);

                Log.d(TAG, "show data: " + user.getUserTitle());
                Log.d(TAG, "show data: " + user.getUserEmail());
                Log.d(TAG, "show data: " + user.getUserType());
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}